import React from 'react'
import { connect } from 'react-redux'
import { fetchUser, setUser, fetchTasks } from '../store/actions'

import Header from '../layout/header'
import Tasks from '../tasks/tasks'

class Content extends React.Component {
    constructor(props) {
        super(props);

		this.state = {
            userDataEditFormVisible: false,
            userName: '',
            userPhone: '',
            userEmail: '', 
            userWebsite: ''
        }

        this.showUserDataEditForm = this.showUserDataEditForm.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePhone = this.handleChangePhone.bind(this);
        this.handleChangeWebsite = this.handleChangeWebsite.bind(this);
        this.handleChangeUserData = this.handleChangeUserData.bind(this);
    }

    componentDidMount() {
        //  ID текущего пользователя, для примера
        let currentUserId = 1;

        if (!this.props.userData)
            this.props.fetchUser(currentUserId);

        if (!this.props.tasksList)
            this.props.fetchTasks(currentUserId);
    }

    showUserDataEditForm() {
        this.setState({
            userName: this.props.userData.name,
            userPhone: this.props.userData.phone,
            userEmail: this.props.userData.email,
            userWebsite: this.props.userData.website,
            userDataEditFormVisible: true
        });
    }

    handleChangeName(event) {
        this.setState({userName: event.target.value});
    }

    handleChangeEmail(event) {
        this.setState({userEmail: event.target.value});
    }

    handleChangePhone(event) {
        this.setState({userPhone: event.target.value});
    }

    handleChangeWebsite(event) {
        this.setState({userWebsite: event.target.value});
    }

    handleChangeUserData(event) {
        event.preventDefault();

        this.props.setUser({
            ...this.props.userData,
            name: this.state.userName,
            phone: this.state.userPhone,
            email: this.state.userEmail,
            website: this.state.userWebsite
        });

        this.setState({
            userDataEditFormVisible: false
        })
    }
  
    render() {
        return <>
            <Header activePage='profile' />
            <div className="container">
                {this.props.userData &&
                    <>
                        <h2>Задачи {this.props.userData.name}</h2>
                        <div className="row">
                            <div className="col-md-4">
                                {this.state.userDataEditFormVisible ? 
                                    <form onSubmit={this.handleChangeUserData}>
                                        <div className="form-group">
                                            <label for="name">Имя</label>
                                            <input type="text" className="form-control" placeholder="Имя" value={this.state.userName} onChange={this.handleChangeName} />
                                        </div>
                                        <div className="form-group">
                                            <label for="phone">Телефон</label>
                                            <input type="text" className="form-control" placeholder="Телефон" value={this.state.userPhone} onChange={this.handleChangePhone} />
                                        </div>
                                        <div className="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" className="form-control" placeholder="Email" value={this.state.userEmail} onChange={this.handleChangeEmail} />
                                        </div>
                                        <div className="form-group">
                                            <label for="website">Сайт</label>
                                            <input type="text" className="form-control" placeholder="Сайт" value={this.state.userWebsite} onChange={this.handleChangeWebsite} />
                                        </div>
                                        <button type="submit" className="btn btn-primary">Сохранить</button>
                                    </form> :
                                    <>
                                        <p>
                                            {this.props.userData.name}
                                        </p>
                                        <p>
                                            {this.props.userData.phone}
                                        </p>
                                        <p>
                                            {this.props.userData.email}
                                        </p>
                                        <p>
                                            <a href={this.props.userData.website}>{this.props.userData.website}</a>
                                        </p>
                                        <p>
                                            <button type="button" className="btn btn-primary" onClick={this.showUserDataEditForm}>Редактировать</button>
                                        </p>
                                    </>
                                }
                            </div>
                            <div className="col-md-8">
                                {(this.props.tasksList && Object.keys(this.props.tasksList).length > 0) &&
                                    <Tasks />
                                }
                            </div>
                        </div>
                    </>
                }
            </div>
        </>;
    }
}

const mapStateToProps = function(state) {
    return {
        userData: state.user.data,
        userFetching: state.user.fetching,
        userError: state.user.error,
        tasksList: state.tasks.list,
        tasksFetching: state.tasks.fetching,
        tasksError: state.tasks.error,
        currentUserId: state.currentUserId
    }
}

const mapDispatchToProps = dispatch => ({
    fetchUser: data => {
        dispatch(fetchUser(data));
    },
    setUser: data => {
        dispatch(setUser(data));
    },
    fetchTasks: data => {
        dispatch(fetchTasks(data));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Content);