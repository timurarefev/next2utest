import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }
  
    render() {
        let tasksClasses = ['nav-item'];
        let profileClasses = ['nav-item'];

        if (this.props.activePage == 'tasks')
          tasksClasses.push('active');
        else if (this.props.activePage == 'profile') 
         profileClasses.push('active');

        return <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className={classNames(tasksClasses)}>
                <a className="nav-link" href="tasks.html">Все задачи</a>
              </li>
              <li className={classNames(profileClasses)}>
                <a className="nav-link" href="profile.html">Мои задачи</a>
              </li>
            </ul>
            {this.props.userData &&
              <span className="navbar-text">
                {this.props.userData.name}
              </span>
            }
          </div>
        </nav>;
    }
}

const mapStateToProps = function(state) {
  return {
    userData: state.user.data,
    userFetching: state.user.fetching,
    userError: state.user.error
  }
}

export default connect(mapStateToProps, null)(Header);