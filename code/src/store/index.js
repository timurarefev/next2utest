import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import axios from 'axios';
import { createRequestInstance, watchRequests } from 'redux-saga-requests';
import { createDriver } from 'redux-saga-requests-axios';

import { userReducer, tasksReducer } from './reducers';

function* rootSaga(axiosInstance) {
    yield createRequestInstance({ driver: createDriver(axiosInstance) });
    yield watchRequests();
}

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
    (typeof window !== 'undefined' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
    compose;

const reducers = combineReducers({
    user: userReducer,
    tasks: tasksReducer
});

const store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(sagaMiddleware))
)

const axiosInstance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
});

sagaMiddleware.run(rootSaga, axiosInstance);

export default store;