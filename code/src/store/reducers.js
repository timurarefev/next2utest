import { FETCH_USER, SET_USER, FETCH_TASKS } from './constants';
import { success, error, abort } from 'redux-saga-requests';

//  Это мы делаем для демонстрации изменения данных на других страницах
let userFromLocalStorage = localStorage.getItem("user");

userFromLocalStorage = (userFromLocalStorage ? JSON.parse(userFromLocalStorage) : false);

const userInitialState = {
    data: (typeof(window.__DATA__) != 'undefined' && typeof(window.__DATA__.user) != 'undefined' ? window.__DATA__.user : userFromLocalStorage),
    fetching: false,
    error: false
}

const tasksInitialState = {
    list: (typeof(window.__DATA__) != 'undefined' && typeof(window.__DATA__.tasks) != 'undefined' ? window.__DATA__.tasks : false),
    users: (typeof(window.__DATA__) != 'undefined' && typeof(window.__DATA__.users) != 'undefined' ? window.__DATA__.users : false),
    fetching: false,
    error: false
}

if (
    tasksInitialState.list && 
    tasksInitialState.list.length > 0 && 
    tasksInitialState.users && 
    tasksInitialState.users.length > 0
)
{
    tasksInitialState.list = tasksInitialState.list.map(item => {
        let users = tasksInitialState.users.filter(user => user.id == item.userId);

        item.userName = '';

        if (users.length == 1)
            item.userName = users[0].name;

        return item;
    });
}

export const tasksReducer = (state = tasksInitialState, action) => {
    switch (action.type) {
        case FETCH_TASKS:
            return { ...tasksInitialState, fetching: true };
        case success(FETCH_TASKS):
            let tasksList = action.data[0].map(item => {
                let users = action.data[1].filter(user => user.id == item.userId);

                item.userName = '';

                if (users.length == 1)
                    item.userName = users[0].name;

                return item;
            });

            return {
                ...tasksInitialState,
                list: tasksList
            };
        case error(FETCH_TASKS):
            return { ...tasksInitialState, error: true };
        case abort(FETCH_TASKS):
            return { ...tasksInitialState, fetching: false };
        default:
            return state;
    }
}

export const userReducer = (state = userInitialState, action) => {
    switch (action.type) {
        case FETCH_USER:
            return { ...userInitialState, fetching: true };
        case success(FETCH_USER):
            return {
                ...userInitialState,
                data: { ...action.data[0][0] },
            };
        case error(FETCH_USER):
            return { ...userInitialState, error: true };
        case abort(FETCH_USER):
            return { ...userInitialState, fetching: false };
        case SET_USER:
            //  Это мы делаем для демонстрации изменения данных на других страницах
            localStorage.setItem("user", JSON.stringify({ ...action.data }));

            return {
                ...userInitialState,
                data: { ...action.data },
            };
        default:
            return state;
    }
}