import { FETCH_USER, SET_USER, FETCH_TASKS } from './constants';

export const fetchUser = id => ({
  type: FETCH_USER,
  request: [{ url: `/users`, params: {id: id}}]
});

export const setUser = data => ({
  type: SET_USER,
  data
});

export const fetchTasks = id => ({
  type: FETCH_TASKS,
  request: [{ url: `/todos`, params: {userId: id}}, { url: `/users`, params: {id: id}}]
});