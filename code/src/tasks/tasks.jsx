import React from 'react'
import { connect } from 'react-redux'

class Task extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <>
            <li className="list-group-item d-flex justify-content-between align-items-center">
                <h5 className="mb-1">
                    {this.props.task.completed ?
                        <s>{this.props.task.title}</s> :
                        this.props.task.title
                    }
                </h5>
                <small>
                    {this.props.task.userId == this.props.userData.id ? this.props.userData.name : this.props.task.userName}
                </small>
            </li>
        </>;
    }
}

class Tasks extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <ul className="list-group">
            {Object.keys(this.props.tasksList).map((taskKey, i) => <Task key={i} userData={this.props.userData} task={this.props.tasksList[taskKey]} />)}
        </ul>;
    }
}

const mapStateToProps = function(state) {
    return {
        userData: state.user.data,
        userFetching: state.user.fetching,
        userError: state.user.error,
        tasksList: state.tasks.list,
        tasksUsersList: state.tasks.users,
        tasksFetching: state.tasks.fetching,
        tasksError: state.tasks.error
    }
}

export default connect(mapStateToProps, null)(Tasks);