import React from 'react'
import { connect } from 'react-redux'
import { fetchUser, fetchTasks } from '../store/actions'

import Header from '../layout/header'
import Tasks from './tasks'

class Content extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //  ID текущего пользователя, для примера
        let currentUserId = 1;

        if (!this.props.userData)
            this.props.fetchUser(currentUserId);

        if (!this.props.tasksList)
            this.props.fetchTasks();
    }
  
    render() {
        return <>
            <Header activePage='tasks' />    
            {(this.props.userData && this.props.tasksList && Object.keys(this.props.tasksList).length > 0) &&
                <div className="container">
                    <h2>Все задачи</h2>
                    <Tasks />
                </div>
            }
        </>;
    }
}

const mapStateToProps = function(state) {
    return {
        userData: state.user.data,
        userFetching: state.user.fetching,
        userError: state.user.error,
        tasksList: state.tasks.list,
        tasksFetching: state.tasks.fetching,
        tasksError: state.tasks.error,
        currentUserId: state.currentUserId
    }
}

const mapDispatchToProps = dispatch => ({
    fetchUser: data => {
        dispatch(fetchUser(data));
    },
    fetchTasks: data => {
        dispatch(fetchTasks(data));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Content);